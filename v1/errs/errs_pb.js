// source: v1/errs/errs.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.fcp.errs.v1.errs.ERROR', null, global);
/**
 * @enum {number}
 */
proto.fcp.errs.v1.errs.ERROR = {
  UNSPECIFIED: 0,
  COMMON_REQUEST_REQUIRED: 1,
  COMMON_INVALID_ARGUMENT: 2,
  COMMON_USER_REQUIRED: 3,
  COMMON_FIELD_REQUIRED: 4,
  COMMON_WRONG_SORT_DATA: 5,
  COMMON_SORTING_BY_FIELD_FORBIDDEN: 6,
  COMMON_DB_ERROR: 7,
  COMMON_DB_CONSTRAIN: 8,
  COMMON_NOT_FOUND: 9,
  ACCOUNTING_INTERNAL: 1000,
  DICTIONARY_INTERNAL: 2000,
  IMAGE_INTERNAL: 3000,
  LANGUAGE_INTERNAL: 4000,
  MESSAGE_INTERNAL: 5000,
  MESSAGE_MOBILE_INTERNAL: 6000,
  MESSAGE_WEB_INTERNAL: 7000,
  ORDER_INTERNAL: 8000
};

goog.object.extend(exports, proto.fcp.errs.v1.errs);
